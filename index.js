// This code will help us access contents of express module/package
	// A "module" is a software component or part of a program that contains one or more routines
// It also allows us to access methods and functions that we will use to easily create an app/server
// We store oure express module to variable so we could easily access its keywords, functions, and methods.
const express = require("express");

// This code creates an application using express / a.k.a express application
	// App is our server
const app = express();

// For our applications erver to run, we need a port to listen to
const port = 3000;

// For our application could read json data 
app.use(express.json());

// Allows your app to read data from forms
// By default, information received from the url can only be received as string or an array
// By applyhing the option of "extended:true" this allows us to receive information in other data types, such as an object which we will use throughout our application
app.use(express.urlencoded({extended:true}));

// This route expects to receive a GET request at the URI/endpoint "/hello"
app.get("/hello", (request, response)=>{

	// This is the response that we will expect to receive if the get method with the right endpoint is successful 
	response.send("GET method success. \nHello from /hello endpoint!");

});

app.post("/hello", (request, response) => {

	response.send(`Hello there ${request.body.firstName} ${request.body.lastName}!`);
});

let users = [];

app.post("/signup", (request, response)=>{
	if(request.body.userName !== "" && request.body.password !== ""){
		users.push(request.body);
		console.log(users);
		response.send(`User ${request.body.userName} succesfully registered`);

		// only the first respoonse will display
		// response.send('Welcome admin')
	}
	else{
		response.send("Please input BOTH username and password");
	}
});

//syntax
/*app.httpMethod("/endpoint", (request, response)=>{

	// code block
});

*/


// we use tput method to update 
app.put("/change-password", (request, response)=>{

	let message;
	// intialization //condition //change of value/index
	for(let i=0; i < users.length; i++){
		// the purpose of our loop is to check our per element in users array if it matc our request.body.username

		// we check if the user is exisiting in our user array
		if (request.body.userName == users[i].userName){

			// update and element objects's password based on the input on the body 
			users[i].password = request.body.password

			// we reassign what message we would luke to receive as respose
			message = `User ${request.body.userName}s password has been updated`;
			break;
		}
		else{

			// if thhere is no match we could like to receive a message that user does not exist
			message = "User does not exist. "
		}
	}

	// we display our updated array
	console.log(users);

	// we display our response based if there is a match (successfully update the password) or if there no match(user does not exist)
	response.send(message);


});

// 1 
// create a rout that expects get request at the URI/endpoint "/home"
// Response that will be received should be "Welcome to the homepage"
//code below...

app.get("/home", (request, response)=>{
	response.send("Welcome to the homepage!");
});


//2
// Create a route that exptects GET request at the URI/endpoint "/users" 
// response that will be receive is a collection of users
//code bellow

app.get("/users", (request, response)=>{
	response.send(users)
});


//3 
// create a rout that expect DELETE request at the URI/endpoint "/delete-user"
// the program should check if the user is exisiting before deleting 
//response should check if the use is exisiting before deleting
//Response that will be receveied is the update collection of users 
// code bellow...

app.delete('/delete-user', (request, response) => {
    for (let i=0; i<users.length; i++) {
       if(users[i].username === request.body.username) {
       users.splice(i, 1);
       console.log(users);
       response.send(users);
       break;
        }
    }
	
});


// 4
// Save all the successful request tabs and export the collection
// Update your GitLab repo and link it to Boodle

// Tells our server/application to listen to the port
// If the port is accessed, we can run the server
// Returns a message to confirm that the server is running in the terminal

app.listen(port, () => console.log(`Server running at port ${port}`))
